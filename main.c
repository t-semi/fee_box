#include "device.h"
#include "music.h"


void init(void);
uint16_t get_ADC_value(uint8_t channel);


int main(void)
{
	init();

	beep_sound(A, 1000);
	while (1);

	while (1) {
		if (get_ADC_value(1) < 60) beep_sound(A, 1000);//play_music(&chime, 1);
		//_delay_ms(50);
	}

	return 0;
}

void init(void)
{
	//GPIO
	DDRB = 0b00000011;

	//set sleep mode to power down mode and set INT0 to rising edge mode
//	MCUCR = 0b00010000;

	//Timer
	TCCR0A = 0b01000010;
	TCCR0B = 0b00000000;
	TIMSK0 = 0b00000000;

	//ADC
	ADMUX = 0b00000000;
	ADCSRA = 0b10000111;
	ADCSRB = 0b00000000;
	DIDR0 = 0b00000100;
}

ISR(INT0_vect)
{

}

uint16_t get_ADC_value(uint8_t channel)
{
	ADMUX &= ~0b11;
	ADMUX |= channel & 0b00000011;

	ADCSRA |= (1 << 6);
	while (ADCSRA & 1 << 6);

	uint16_t value = (uint16_t) (ADCH << 8 | ADCL);

	return value;
}
