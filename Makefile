# 

MCU		:= attiny13a
TARGET_MCU	:= t13
CLOCK		:= 9600000UL

PROGRAMMER	:= avrispmkII
DEBUGGER	:= avrispmkII

AVR		:= avr
CC		:= $(AVR)-gcc
CXX		:= $(AVR)-g++
LD		:= $(AVR)-ld
DEBUGGER	:= $(AVR)-gdb
SIZE		:= $(AVR)-size
READELF		:= $(AVR)-readelf
OBJCOPY		:= $(AVR)-objcopy
OBJDUMP		:= $(AVR)-objdump
WRITE		:= avrdude

buildtype	:= debug
DEBUG_FLAGS	:= -g -Os -DDEBUG
RELEASE_FLAGS	:= -Os -DNDEBUG

SUB_DIRECTORY	:= ./common

CFLAGS		:= -std=c99 -Wall -Wextra -Wno-unused-parameter #-Wconversion
CFLAGS		+= -mmcu=$(MCU) -DF_CPU=$(CLOCK)
CFLAGS		+= -D_GNU_SOURCE

CXXFLAGS	:= -std=c++14 -Wall -Wextra -Wno-unused-parameter #-Wconversion
CXXFLAGS	+= -mmcu=$(MCU)
CXXFLAGS	+= -D_GNU_SOURCE

SOURCES_DIR	:= ./
SOURCES_C	:= $(wildcard $(SOURCES_DIR)*.c)
SOURCES_C	+= $(wildcard $(SUB_DIRECTORY)/*.c)
SOURCES_ASM	:= $(wildcard $(SOURCES_DIR)*.s)
SOURCES_ASM	+= $(wildcard $(SUB_DIRECTORY)/*.s)


OBJ_DIR		:= ./obj
OBJECTS		:= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.c,%.o,$(SOURCES_C))))
OBJECTS		+= $(addprefix $(OBJ_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.s,%.o,$(SOURCES_ASM))))

DEP_DIR		:= ./obj
DEPENDS		:= $(addprefix $(DEP_DIR)/$(buildtype)/, $(subst ./,,$(patsubst %.c,%.d,$(SOURCES_C))))


INCLUDE		:= -I./
INCLUDE		+= -I./$(SUB_DIRECTORY)

LIBS_DIR	:= 
LIBS		:= 


TARGET_DIR	:= ./bin
TARGET_NAME	?= target
TARGET_ELF	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).elf
TARGET_BIN	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).bin
TARGET_HEX	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).hex
TARGET_LIST	:= $(TARGET_DIR)/$(buildtype)/$(TARGET_NAME).list
TARGET		:= $(TARGET_ELF)
TARGETS		:= $(TARGET_ELF) $(TARGET_BIN) $(TARGET_HEX) $(TARGET_LIST)

GDB_COMMAND_FILE	:= ./$(SUB_DIRECTORY)/autowrite
GDB_DEBUG_FILE		:= ./$(SUB_DIRECTORY)/autodebug


ifeq ($(buildtype), debug)
	CFLAGS += $(DEBUG_FLAGS)
else ifeq ($(buildtype), release)
	CFLAGS += $(RELEASE_FLAGS)
else
	$(echo buildtype must be debug or release)
	$(exit 1)
endif


all : build size

build : $(TARGETS)

run : $(TARGET) #$(GDB_COMMAND_FILE)
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U flash:w:$(TARGET_HEX) -v
#	$(DEBUGGER) $< -batch -nx -x $(GDB_COMMAND_FILE)

write : $(TARGET_HEX) #$(GDB_COMMAND_FILE)
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U flash:w:$(TARGET_HEX) -v
#	$(DEBUGGER) $< -batch -nx -x $(GDB_COMMAND_FILE)

writefuse:
	$(WRITE) -c $(PROGRAMMER) -p $(TARGET_MCU) -U lfuse:w:0x7a:m

debug : $(TARGET) $(GDB_DEBUG_FILE)
	$(DEBUGGER) $(TARGET) -nx -x $(GDB_DEBUG_FILE)

-include $(DEPENDS)


$(TARGET_ELF) : $(OBJECTS)
	mkdir -p `dirname $@`
	$(CC) $^ $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@ -Wl,-M=$(TARGET).map


$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.c Makefile
	mkdir -p `dirname $@`
	$(CC) -c -MMD -MP $< $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(OBJ_DIR)/$(buildtype)/%.o : $(SOURCES_DIR)/%.s Makefile
	mkdir -p `dirname $@`
	$(CC) -c -MMD -MP $< $(CFLAGS) $(INCLUDE) $(LIBS_DIR) $(LIBS) -o $@

$(STARTUP_OBJ) : $(STARTUP_ASM) Makefile
	mkdir -p `dirname $@`
	$(CC) -c $< $(CFLAGS) -o $@

%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

%.bin: %.elf
	$(OBJCOPY) -O binary $< $@

%.list: %.elf
	$(OBJDUMP) -x -S $< > $@

size: $(TARGET)
	@echo ""
	$(SIZE) $<
	@echo ""

list : $(TARGET_LIST)

clean :
	rm -f -r $(TARGETS) $(OBJECTS) $(DEPENDS) $(TARGET_DIR) $(OBJ_DIR)

.PHONY: all build run write debug size list clean
