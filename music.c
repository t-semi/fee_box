#include "device.h"

#include "music.h"
#include "titles.h"

const uint16_t Note_Freq[] = {
	0, 26163, 27718, 29366, 31112, 32963, 34923,
	36999, 39200, 41530, 44000, 46616, 49388
};


void play_music(music *title, int times)
{
	uint16_t melody_length = 0;
	const uint8_t (*p_music)[3];

	p_music = title->score;
	uint16_t interval = set_tempo(title->tempo);

	melody_length = title->length;

	while (times--) {
		while (1) {
			uint16_t note_count = 0;

			set_musical_freq(p_music[note_count][0], p_music[note_count][1]);
			TCCR0B |= 0b011;
			delay_ms(interval * p_music[note_count][2]);
			TCCR0B &= (uint8_t) ~0b011;
			note_count++;

			if (note_count > melody_length) {
				TCCR0B &= (uint8_t) ~0b100;
				break;
			}
		}
	}
	return;
}

void beep_sound(CODE_NOTE code_note, uint16_t msec)
{
	set_musical_freq(code_note, 8);
	TCCR0B |= 0b011;
	delay_ms(msec);
	TCCR0B &= (uint8_t) ~0b011;
}
