#ifndef MUSIC_H
#define MUSIC_H

#define set_musical_freq(interval, octave) OCR0A = \
	(uint16_t) ((1500000 / (Note_Freq[(interval)] * (octave))) / 2)
//96000000 / 64 * 100 = 1500000

#define set_tempo(tempo)  (uint16_t) ((60000) / ((tempo) * 24))

typedef enum {
	REST, C, C_, D, D_, E, F, F_, G, G_, A, A_, B
} CODE_NOTE;

typedef enum {
	SIXTEENTH_NOTE = 6, DOTTEDSIXTEENTH_NOTE = 9, EIGHTH_NOTE = 12, TRIPLET = 16,
			DOTTED_EIGHTH_NOTE = 18, QUARTER = 24, DOTTED_QUARTER = 36,
			HALF = 48, WHOLE = 96
} NOTE_LENGTH;

typedef const struct {
	uint8_t tempo;
	uint16_t length;
	uint8_t score[][3];
} music;

extern music chime;

extern const uint16_t Note_Freq[];
extern uint8_t sound_status;

void play_music(music *title, int times);
void beep_sound(CODE_NOTE code_note, uint16_t msec);

#endif /* MUSIC_H */
