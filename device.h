#ifndef DEVICE_H
#define DEVICE_H

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdint.h>
#include <stdbool.h>

#include "utility.h"
#include "port.h"

#define CPU_FREQ 9600000

struct st_pa {
	union {
		unsigned char BYTE;
		struct {
			unsigned char BIT0:1;
			unsigned char BIT1:1;
			unsigned char BIT2:1;
			unsigned char BIT3:1;
			unsigned char BIT4:1;
			unsigned char BIT5:1;
			unsigned char BIT6:1;
			unsigned char BIT7:1;
		} BIT;
	} PIN;

	unsigned char DDR;
	union {
		unsigned char BYTE;
		struct {
			unsigned char BIT0:1;
			unsigned char BIT1:1;
			unsigned char BIT2:1;
			unsigned char BIT3:1;
			unsigned char BIT4:1;
			unsigned char BIT5:1;
			unsigned char BIT6:1;
			unsigned char BIT7:1;
		} BIT;
	} PORT;
};

//#define IOA	(*(volatile struct st_pa *)0x20) // PA Address
#define IOB	(*(volatile struct st_pa *)0x23) // PB Address
//#define IOC	(*(volatile struct st_pa *)0x26) // PC Address
//#define IOD	(*(volatile struct st_pa *)0x29) // PD Address

#endif /* DEVICE_H */
