#ifndef CHIME_H
#define CHIME_H

music chime = {120, 16,
	{
		{F, 4, QUARTER},
		{A, 8, QUARTER},
		{G, 4, QUARTER},
		{C, 4, 27*3},
		{F, 4, QUARTER},
		{G, 4, QUARTER},
		{A, 8, QUARTER},
		{F, 4, 27*3},
		{A, 8, QUARTER},
		{F, 4, QUARTER},
		{G, 4, QUARTER},
		{C, 4, 27*3},
		{C, 4, QUARTER},
		{G, 4, QUARTER},
		{A, 8, QUARTER},
		{F, 4, 27*3}
	}};
#endif /* CHIME_H */

